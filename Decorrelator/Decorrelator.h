#pragma once

#include "TH1D.h"
#include "TH2D.h"
#include <TMatrixDSym.h>

#include <vector>
#include <string>

class Decorrelator {
public:
    /**
     * Constructor
     */
    explicit Decorrelator();

    /**
     * Default destructor
     */
    ~Decorrelator() = default;

    /**
     * Remove all move/assignment/copy constructors
     */
    Decorrelator(const Decorrelator& d) = delete;
    Decorrelator operator=(const Decorrelator& d) = delete;
    Decorrelator(Decorrelator&& d) = delete;
    Decorrelator operator=(Decorrelator&& d) = delete;

    /**
     * Function to prepare histograms for event loop processing
     * @param bins number of bins
     * @param min min X
     * @param max max X
     */
    void PrepareHistogram(int bins, float min, float max);

    /**
     * Function to add a single event values (for event loop)
     * @param values the vector of values, one element for each variable
     * the size has to agree with the order in which the order of the initialized histograms
     */ 
    void AddEvent(const std::vector<float>& values);

    /**
     * Function to get the eigenvectors;
     * @return returns 2D vector, first dimension reprensets the eigenvectors
     * the second dimension represents the actual values
     */
    std::vector<std::vector<float> > GetEigenVectors() const;

    /**
     * Function to get covariances of the parameters
     * @return returns the ordered set of covarariance values for pair of the observables
     */
    std::vector<float> GetCovariances() const;

    /**
     * Function to get correlations of the parameters
     * @return returns the ordered set of correlation values for pair of the observables
     */
    std::vector<float> GetCorrelations() const;

    /**
     * Function to return the individual 2D correlation histograms for each pair of the observables
     * @return returns vector of TH2D histograms for each pair of the observables
     */
    const std::vector<TH2D>& GetCorrelationHistograms() const {return m_correlationHistograms;}

    /**
     * Function to get the final covariance matrix between the observables
     * @return returns the TMatrixDSym object containing the covaraince matrix of the variables
     */
    TMatrixDSym GetCovarianceMatrix() const;

private:

    struct Hist {
        int bins;
        float min;
        float max;
    };

    std::vector<TH2D> m_correlationHistograms;
    std::vector<Hist> m_singleHistograms;
    std::vector<TH1D> m_variances;
    bool m_useEventLoop;
    bool m_useHistograms;
};
