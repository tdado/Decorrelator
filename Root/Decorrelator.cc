#include "Decorrelator/Decorrelator.h"

#include <TMatrixDSymEigen.h>

#include <algorithm>
#include <exception>

Decorrelator::Decorrelator() :
m_useEventLoop(false),
m_useHistograms(false)
{
}

void Decorrelator::PrepareHistogram(int bins, float min, float max) {
    if (m_useHistograms) {
        throw std::invalid_argument{"Decorrelator::PrepareHistogram: Trying to prepare histograms for event loop but already using the histogram inputs"};
    }
    m_useEventLoop = true;
    m_variances.emplace_back("","",bins, min, max);
    for (const auto& ihist : m_singleHistograms) {
        m_correlationHistograms.emplace_back("","",ihist.bins, ihist.min, ihist.max, bins, min, max);
    }
    Decorrelator::Hist hist;
    hist.bins = bins;
    hist.min = min;
    hist.max = max;
    m_singleHistograms.emplace_back(std::move(hist)); 
}

void Decorrelator::AddEvent(const std::vector<float>& values) {
    if (m_useHistograms) {
        throw std::invalid_argument{"Decorrelator::AddEvent: Trying to add an event in an event loop but already using the histogram inputs"};
    }

    if (values.size() < 2) {
        throw std::invalid_argument{"Decorrelator::AddEvent: Need more than 1 value in the AddEvent function"};
    }

    const std::size_t allHistos = values.size() * (values.size() - 1) / 2;

    if (m_correlationHistograms.size() != allHistos) {
        throw std::invalid_argument{"Decorrelator::AddEvent: Inconsistent size of initialized histograms and provided values"};
    }

    std::size_t n{0};
    for (std::size_t i = 0; i < values.size(); ++i) {
        for (std::size_t j = i + 1; j < values.size(); ++j) {
            m_correlationHistograms.at(n).Fill(values.at(i), values.at(j));
            n++;
        }
        m_variances.at(i).Fill(values.at(i));
    }
}

std::vector<std::vector<float> > Decorrelator::GetEigenVectors() const {
    TMatrixDSym matrix = this->GetCovarianceMatrix();
    // get the eigenvector
    TMatrixDSymEigen eigen(matrix);
    TMatrixD eigenvec = eigen.GetEigenVectors();

    std::vector<std::vector<float> > result;
    for (std::size_t i = 0; i < m_singleHistograms.size(); ++i) {
        std::vector<float> line;
        bool flip{true};
        float largest{0};
        for (std::size_t j = 0; j < m_singleHistograms.size(); ++j) {
            const double eigenElement = eigenvec(j,i);
            line.emplace_back(eigenElement);
            if (eigenElement > 0) flip = false;
            if (std::abs(eigenElement) > std::abs(largest)) {
                largest = eigenElement;
            }
        }
        if (flip || largest < 0) {
            // we can multiply the vector by a constant and it will still be an eigenvector
            // in this case multiply by -1 to get the more natural result
            std::transform(line.begin(), line.end(), line.begin(), [](float element){return -1*element;});
        }
        result.emplace_back(line);
    }

    return result;
}

std::vector<float> Decorrelator::GetCorrelations() const {
    std::vector<float> result;
    for (const auto& ihist : m_correlationHistograms) {
        result.emplace_back(ihist.GetCorrelationFactor());
    }

    return result;
}

std::vector<float> Decorrelator::GetCovariances() const {
    std::vector<float> result;
    for (const auto& ihist : m_correlationHistograms) {
        result.emplace_back(ihist.GetCovariance());
    }

    return result;
}

TMatrixDSym Decorrelator::GetCovarianceMatrix() const {
    const std::size_t size = m_singleHistograms.size();
    TMatrixDSym matrix(size);

    // fill the matrix
    std::size_t n{0};
    for (std::size_t i = 0; i < size; ++i) {    
        for (std::size_t j = i + 1; j < size; ++j) {
            const double cov = m_correlationHistograms.at(n).GetCovariance();
            matrix(i,j) = cov;
            matrix(j,i) = cov;
            n++;
        }
    }

    for (std::size_t i = 0; i < size; ++i) {
        const double variance = m_variances.at(i).GetRMS();
        matrix(i,i) = variance * variance;
    }

    return matrix;
}