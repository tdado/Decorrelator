#include "Decorrelator/Decorrelator.h"

#include "TFile.h"
#include "TTree.h"

#include <iostream>
#include <vector>

int main (int /*argc*/, char** /*argv*/) {

    // read some inputs for testing
    TFile f("data/example.root","read");
    TTree* tree = static_cast<TTree*>(f.Get("tree"));

    float pt, eta, phi;
    tree->SetBranchAddress("pt",&pt);
    tree->SetBranchAddress("eta",&eta);
    tree->SetBranchAddress("phi",&phi);

    Decorrelator decor{};
    // prepare the histograms
    decor.PrepareHistogram(100, 25, 200);
    decor.PrepareHistogram(100, -2.5, 2.5);
    decor.PrepareHistogram(100, -3.14, 3.14);

    // event loop
    for (int ievent = 0; ievent < tree->GetEntries(); ++ievent) {
        tree->GetEvent(ievent);
        std::vector<float> values;
        values.emplace_back(pt/1e3);
        values.emplace_back(eta);
        values.emplace_back(phi);

        decor.AddEvent(values);
    }

    const std::vector<float>& correlationsBefore = decor.GetCorrelations();
    std::cout << "\nCorrelations BEFORE the decorrelation\n";
    for (const auto& i : correlationsBefore) {
        std::cout << i << " ";
    }
    std::cout << "\n";

    // get the eigenvectors
    const std::vector<std::vector<float> > eigenvectors = decor.GetEigenVectors();
    for (const auto& ieigenVec : eigenvectors) {
        std::cout << "Eigen vector: " << ieigenVec.at(0) << " " << ieigenVec.at(1) << " " << ieigenVec.at(2) << "\n";
    }

    // now test this 
    // we dont need the Decorrelator class for this but it is convenient
    Decorrelator decorTest{};
    decorTest.PrepareHistogram(1000, -200, 200);
    decorTest.PrepareHistogram(1000, -200, 200);
    decorTest.PrepareHistogram(1000, -200, 200);

    // event loop to test the changes
    for (int ievent = 0; ievent < tree->GetEntries(); ++ievent) {
        tree->GetEvent(ievent);
        std::vector<float> values;
        // create the linear combinations of the input variables
        for (const auto& ieigenVec : eigenvectors) {
            const float value = ieigenVec.at(0)*pt/1e3 + ieigenVec.at(1)*eta + ieigenVec.at(2)*phi;
            values.emplace_back(value);
        }

        decorTest.AddEvent(values);
    }

    const std::vector<float>& correlations = decorTest.GetCorrelations();
    std::cout << "\nCorrelations AFTER the decorrelation\n";
    for (const auto& i : correlations) {
        std::cout << i << " ";
    }
    std::cout << "\n";
}
